

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="./standalone/jquery-3.3.1.slim.js"></script>
    <script src="./standalone/microplugin.js"></script>
    <script src="./standalone/sifter.js"></script>
    <script src="./standalone/selectize.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
      <link rel=stylesheet href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <link rel=stylesheet href="selectize.css">
  
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Stone - ITAD Booking Form</title>
    
</head>
<body>


<p> <?PHP 
// $timestamp = date('Y-m-d G:i:s'); 

// echo $timestamp ; 


require_once('db.php');


$time = date("Y-m-d H:i:s");


$username = php_uname('n');
$sqlog = "

insert into [dbo].[RSAS_logs]
values('".$_SERVER['REQUEST_URI']."', '".$username."', '".$time."') ";


$stmtlog = $conn->prepare($sqlog);
if (!$stmtlog) {
  echo "\nPDO::errorInfo():\n";
  print_r($conn->errorInfo());
  die();
}
$stmtlog->execute();

$log = $stmtlog->fetch(PDO::FETCH_ASSOC);

$fh = fopen("rsatrack.txt","a+");
fwrite($fh,$sqlog."\n");
fclose($fh);



?> </P>



<style>


@font-face {
  font-family: 'stonefont';
  src:  url('NeuzeitGroBold.ttf') format('truetype');
       
}



body {

  font-size: 20px;
}



th {

    font-size:1.5em;

}



td {

font-size:1.4em;

}

.glyphicon {
    font-size: 25px;
}


.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.con {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 60%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}


.plus .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    font-size:1em;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.plus:hover .tooltiptext {
    visibility: visible;
}


.delline .tooltiptext2 {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    font-size:1em;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

  .delline:hover .tooltiptext2 {
    visibility: visible;
}

.header{

 /*background-color:black;*/
 background: url("recyclingform_headerimg.jpg" );
 background-repeat: no-repeat;
  background-size: 100%;
  margin:0;
  border-bottom: 10px solid black;


  
 color:white;
  text-align: center;
     padding:4em;
    



}

h1{
  font-family: 'stonefont';
     font-size:5em;
     color:white;
     text-decoration: underline;
}

h3{

font-size:2em;
font-family: 'stonefont';
}


body{

    font-family: Calibri;
    font-size:16.0pt;
    width:100%;
}



body label{

margin-left:10px;

}

#space{

    padding:20px;
}


#part option{

  font-weight: bold;
}


#pdf p{

  font-size:3em;

}



</style>







<div class='header container-fluid'>

<h1>IT Asset Disposal</h1>
<h3>Book a Collection</h3><br>
<img src='Stone Logo.png' height='70'/>

<br/>
<br/>
</div>

<div class="container">
<br/>
<br/>

<div class="row">


    <form id="formstuff" method="POST" action="updatetables.php" enctype="multipart/form-data">
    <div class="form-group ">
    <div class="col-xL-4">
    <label for="Organisation"> Organisation<span style="color:red;">*</span></label>
    
    <input type="text" id="org" name="Org" class="form-control" placeholder="Organisation Name" /><br>


    <label> Request Contact </label>
    <input type="text" id="reqcon"  class="form-control" placeholder="Request Contact"><br>

        <label>  Position </label>
    <input type="text" id="pos" name="position" class="form-control" placeholder="Position"><br><br>


    <label hidden aligin="center" for="SICcode"> SICcode </label>
    <input type="hidden" id="sicco" name="sicco" value="00000" class="form-control" style="width:10%" size="5" maxlength="5" placeholder="Required field" />
    
    <label for="user_email"> Email Address<span style="color:red;">*</span></label>
    <input type="email" name="contactemail" id="email" class="form-control "  placeholder="Email Address"  /><br><br>

        <label>  Telephone<span style="color:red;">*</span></label>
    <input type="text" id="tel" class="form-control"  /><br><br>

    
    <label for="country">  Country<span style="color:red;">*</span></label>
    <div class="dropdown">
<select class="form-control" id="country" >
  <option value="de" selected>Select Country</option>
    <option value="England">England</option>
    <option value="Wales">Wales</option>
    <option value="Scotland">Scotland</option>
    <option value="Other">Other</option>
    <br/>

</select>
</div>
<br/>
<br/>
<div id='premisestuff'>
        <label style="color: #FF0000">  **Hazardous Waste Producers
Premises Code </label>
    <input type="text" name="premisescode" id="prem" class="form-control"  /><br>
    <span style="font-size:12.0pt;line-height:115%; font-family: Calibri;">
                            If you produce more than 500Kg annually of any Waste classified as Hazardous and the site producing the waste is located in Wales; you are legally required to register your site as a producer with National Resources Wales &amp; receive a site Premises Code. Please tick exempt if less than 500Kg.
                            Sites in England, Scotland &amp; NI are not required to register, please tick exempt.<br>
                            Click <a href="http://www.environment-agency.gov.uk/business/topics/waste/32198.aspx" target="_blank" style="color: #008000">here</a> for guidance.(Typically IT equipment with a screen is classified as Hazardous).
                        </span>
                        <br/>

                      
                        
    
   
    <span> <label>Exempt</label></span>  <input type="checkbox"  name="ex" id="expmt" > <br><br>
  </div>


<br/>



 

<br/>

</div>
<div class="row">
<div>
    <label>  Address<span style="color:red;">*</span></label><span><br>
    <input type="text" id="add1" name="Address1" class="form-control" placeholder="Address Line 1" /><Br>
    <input type="text" id="add2" class="form-control" placeholder="Address Line 2"><br>
    <input type="text" id="add3" class="form-control" placeholder="Address Line 3"><br>
    <label>  Town<span style="color:red;">*</span></label><span><br> <input type="text" class="form-control" id="twn" placeholder="Town / City" /><br>
    <label>  Postcode<span style="color:red;">*</span></label><span><br> <input type="text" name="Postcode" class="form-control" id="postc" placeholder="Postcode" /> <br> 
    <label>  Site Contact </label>
    <input type="text" id="cont" name="SiteContact" class="form-control" placeholder="Site Contact"><br>

                <label>  Contact Phone<span style="color:red;">*</span></label>
    <input type="text" id="contph" name="contactphone" class="form-control" placeholder="Contact Phone"> <br>
    
    <span style="color: #FF0000"> *A mobile phone number is preferable.  </span>
    <br/>

</div>



<br/> 



</div>






</div>



<table  class="table" id="tb">
<thead>
<tr class="tr-header">

<th> Items </th>
    <th> Qty  </th>
    <th hidden> Qty Not Working </th>
    <th>Asset Management Required </th>
    <th>Enhanced Wipe (Chargeable) </th>
    
<th hidden class="plus"><a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More lines"><span class="glyphicon glyphicon-plus"></span></a>
<span class="tooltiptext">Add a new line</span></th>

</div>
</thead>
<tbody>
<strong>Collection Details</strong>


                  <p>  Please list the equipment for collection in order that Stone can organise appropriate vehicles and economical route planning/collection dates. A collection date will be agreed within 48 hours following receipt of request.</p>


<br/>
<strong>Collection Charges </strong>
<p>In all cases Stone will endeavour to provide a cost neutral service, to make this sustainable, clients are requested to have a minimum of <b>25</b> qualifying items for collection - Qualifying items will highlight once selected. If a collection does not qualify then a £249.00 + VAT charge will be applied. </p>

<tr id="row0">
<td>

<div class="control-group">
<div class="dropdown">
<select name="select-parts" id='part' class="form-control select-parts">
<option value="de" selected><strong>Select Part Type</strong></option>
    <option value="3">PC</option>
    <option value="31">ALLINONE PC</option>
    <option value="5">LAPTOP</option>
    <option value="41">LAPTOP Trolly</option>
    <option value="47">Smart Phone</option>
    <option value="45">Apple Phone</option>
    <option value="49">Apple Tablet</option>
    <option value="51">Tablet</option>
    <option value="11">TFT</option>
    <option value="15">TV</option>
    <option value="7">SERVER</option>
    <option value="43">Network-Switches</option>
    <option value="17">Hard-Drive</option>
    <option value="29">SMARTBOARD</option>
    <option value="25">DESKTOP-PRINTER</option>
    <option value="27">STANDALONE-PRINTER</option>
    <option value="9">PROJECTOR</option>
    <option value="13">CRT</option>

 

</select>
</div>
</div>


</div>





	



</td>
<td><input type='number' min="0" max="5000" class='working'  ></td>
<td hidden><input type='number' min="0" max="5000" class='notworking'  ></td>
<td><input type='checkbox' class='v' class='group1'></td>
  <td><input type='checkbox' class='w' name='wipe' class='group1' > </td>
<td class="delline" hidden><a href='javascript:void(0);'  class='remove'><span class='glyphicon glyphicon-remove'></span></a>
<span class="tooltiptext2">remove a line</span></td>

</tr>
</tbody>
</table>

<p id='charge' style='color:red;'> * this request has less than 25 qualifying items therefore will be chargable </p>

<input type='button' value='Add Line' class="btn btn-primary btn-lg" id='addmore2'>
<br/>



<br/>


<strong>** You can enter an additional 3 items, please note additional items may not be accepted.</strong>
<p> If your product is not in the above list, please add in the box below. </p><br/>
<!-- <p>(e.g. peripherals x 2, Tapes x 10 and total in the Quantity box)</p> -->
<strong> You can only enter: </strong><p><p id="limit"><b>3</b></p>  <b> items </b> </p>
<input type="text" id="newitem" placeholder="e.g. Xbox360"><label><input type="button" class="btn btn-primary btn-lg" id="additem" value="submit"></label>
<br/>
<br/>
<br/>


<div id="imgupload">
    
    <p style="color:green;"><strong> Please provide images of all of the items Collectively – This will help speed up the booking process.</strong>  </p><br>
    
            <input type="file" id="file" name="inputfile[]"  accept=".jpeg,.jpg,.png, .gif" data-type='image'/>
            
            <input type="file" id="file1" name="inputfile[]"  accept=".jpeg,.jpg,.png, .gif" data-type='image' />
            <input type="file" id="file2" name="inputfile[]"  accept=".jpeg,.jpg,.png, .gif" data-type='image' />
            
            
         
       
<div>
   


<br/>
<br/>








   
<div>




    
    <br>
    <label> BIOS password  (Laptop Only – Please note the BIOS password must be removed or provided as part of the collection. If we cannot pass this to wipe the items a charge per unit will be applied) </label>
    <input type="text" class="form-control"  id="biopass" placeholder="BIOS password">

    <strong>(N/A)  </strong>
    <input type="checkbox" id="bioscheck"/>
<br>
    <div>

  
     <BR/>
    <div class="col-xl-4">
         <label>  
Collections are available Monday to Friday (excluding bank holidays). If you have either a preferred collection date or days upon which collection would not be possible, please advise us here: </label>
     <textarea   id="coldate" class="form-control rounded-0" rows="8" cols="70"></textarea>
</div>

<br/>


    <div class="col-xl-4">
    <label> Please advise any specific access or location issues: </label>
     <textarea id="colins" rows="8" class="form-control rounded-0" cols="70" placeholder="e.g. Lift's don't work" ></textarea>
     </div>
  




</div>
<br/>
<div id="confarea"> 
<strong>Data Protection Law</strong>
<br>
Please note that in order to process your Assets we will require your acknowledgment to our Data Protection Law  agreement. You will be directed to this once you submit your request. 
                    <p>
                        Click here to acknowledge confirmation: <input type="checkbox" id="confirmed" value="ON"> </p>
</div>

<!-----<div>
<label> Name </label>
    <input type="text" name="signatory" id="reqcon" STYLE="width:30%" class="form-control" >
<br/>
        <label>  Postion </label>
    <input type="text" id="pos" STYLE="width:30%"  class="form-control" ><br>

</div>---->

<hr>

<input type="submit"  class="btn btn-primary btn-lg" size="20px" id="testbutt" >

<br/>
<br/>

   </form>


<strong>
                            Please contact our Stone ITAD Administration if you require any further assistance:

                            <br><br>E Mail: <a href="mailto:Zoe.Hill@stonegroup.co.uk" style="color: #008000">Zoe.Hill@stonegroup.co.uk </a>  <br>
                            Direct Telephone Number : 01785 786 711
                        </strong>
    
<br/>

<div id="myModal" class="modal">

<div align="center" class='con'>
<span class="close">&times;</span>

  <h1 class="display-3">Standard Agreement</h1><br/>
  <div id="pdf">
  
  <iframe src="OnlineDataProcessingContract.pdf" width="50%" height="600px">
This browser does not support PDFs. Please download the PDF to view it: 
<a href=/OnlineDataProcessingContract.pdf">Download PDF</a></iframe>
<br/><br/>
  
  
  </div>
  <button type="button" id="agree" class="btn btn-success"><b>Agree</b></button>
  <button type="button" id="disagree"  class="btn btn-danger"><b>Disagree</b></button>
  <br/>
  <br/>
  <!-----<a class="btn btn-info btn-sm" href="optionpage.php" role="button">< Back</a>--->



  <hr>
<h2> Bespoke Argeements </h2>
  <p>
   A Bespoke Agreement is specifically tailored for an individual customer.<br>
  If you have one in place please <a href="mailto:Zoe.Hill@stonegroup.co.uk?Subject=GDPR%20Enquires">Email us</a> or Call us on <a href="tel:01785 786729">01785 786729</a>.<br><br><br>

 if not or you are happy to do so please Agree to the Standard Agreement?.   </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="https://www.stonegroup.co.uk/" role="button">Continue to homepage</a>
  </p>

</div>

  
<div id="space">

</div>



<script>
$( document ).ready(function() {
  val = 17;
  var count = 3;
  var othercount = 0;
 
  var is_exempt = 0;

var newline; 
var newline2; 


var pre = [];


var assettick = 0;
var wipetick = 0;
var f_assettick = 0;
var f_wipetick = 0;
var check = 0;



var id = 0;
var i = 0;
 var addlimit = 0;

 var jsonsuperclean = '';
 var cur = '';
 var curname = '';
 var othercheck = 0;

 var filecheck = 0;


 
 $('#myModal').css( "display", "none" );

 $('.close').on('click', function(){
  $('#myModal').css( "display", "none" );

 });


 $('#disagree').on('click', function(){

  if(filecheck == 0){
      deluploadimage();

  }


   alert("Unfortunately, we are unable to submit this request.\n Please agree to our agreement to submit.")
  $('#myModal').css( "display", "none" );

 });

 $('#agree').on('click', function(){
  check = 1;
  uploadimage();
  alert('test');

  $('#testbutt').click();
  

 });



//  $('#but_upload').on('click', function(){

// uploadimage();

//  });

// $('#tb').click(function(){
  
//  arr = [];

 // $('#tb tr').each(function() {

    // var working = Number($(this).find(".working").val());

    // if(working !== undefined){
    //   if (!isNaN(working) && working.length !== 0) {
    //     totalqty += parseFloat(working);

    //               }
    // }




//     console.log(totalqty);



//   });



  


// });




//  var modal = document.getElementById("myModal");

// // Get the button that opens the modal
// var btn = document.getElementById("testbutt");

// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];

// // When the user clicks the button, open the modal 
// btn.onclick = function() {
//   modal.style.display = "block";
// }

// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal.style.display = "none";
// }

// // When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }

$('#premisestuff').hide();


var arr = [];
var newArray = [];
var thepast = [];
var history = [];


var selpoint = '';





// $('select[name=team2]').on('change', function() {
//   console.log('select');

// // $('#tb tr').each(function() {

// // idd = $(this).closest('tr').attr('id');

// // console.log(idd);

// //    var self = $('#row0 select[name=select-parts]').val();
// // console.log(self);
// // $('#row1 select[name=select-parts]').find('option').prop('disabled', function() {
// // console.log(this.value);
// // return this.value == self
// // });

// // if(id > 1){



// // }

// //     });

// });



// if(id > 0){

// alert('yes more than 0');

//    var self = $('#row1 select[name=select-parts]').val();
//     console.log(self);
//  $('#row0 select[name=select-parts]').find('option').prop('disabled', function() {
//    console.log(this.value);
//      return this.value == self
//    });




// }


// if(cur.length){

// $('.select-parts').on('click', function() {
// if(cur !== partselected){
//   alert(cur);
//   alert(curname);
//   $('#tb #row'+id+'').find('.select-parts').append("<option value="+cur+">"+curname+"</option>");

// // });

// }


//}#
$(" .working").prop('disabled', true);
//$(" .notworking").prop('disabled', true);


$('.select-parts').on('change', function() {


  if($('.select-parts').val() !== 'de'){

    
    $(" .working").prop('disabled', false);
   // $(" .notworking").prop('disabled', false);

  }


});




$('#tb').on('input', 'tbody tr', function(event) {


totalqty = 0;

$('#tb tr').each(function() {


  var working = Number($(this).find(".working").val());

if(working !== undefined){
if (!isNaN(working) && working.length !== 0) {
  totalqty += parseFloat(working);

            }
}
console.log(totalqty);


if(totalqty >= 25){

$('#charge').hide();
}else{
$('#charge').show();
}

  });
});





$('#tb').on('click', 'tbody tr', function(event) {


  totalqty = 0;

  $('#tb tr').each(function() {


    var working = Number($(this).find(".working").val());

if(working !== undefined){
  if (!isNaN(working) && working.length !== 0) {
    totalqty += parseFloat(working);

              }
}
console.log(totalqty);


if(totalqty >= 25){

  $('#charge').hide();
}else{
  $('#charge').show();
}

    idd = $(this).closest('tr').attr('id');
console.log(idd);

// //console.log(this);
// $(".working").prop('disabled', false);
// $(".notworking").prop('disabled', false);

// if($(this).find('.select-parts').val() == 'de'){


// $(this).find(".working").prop('disabled', true);
// $(this).find(".notworking").prop('disabled', true);


// }else if($(this).find('.select-parts').val() != 'de'){

// $(this).find(".working").prop('disabled', false);
// $(this).find(".notworking").prop('disabled', false);

// }


$('.select-parts').on('change', function() {

   arr = [];
 newArray = [];


// if($('.select-parts').val() !== 'de'){


// $(".working").prop('disabled', false);
// $(".notworking").prop('disabled', false);


// }else{


// $(".working").prop('disabled', true);
// $(".notworking").prop('disabled', true);

// }

//console.log(idd);




  });


});



//console.log(this);







//selected = $(this).closest('tr').find('.select-parts option:selected').val();

// if(selected !== $(this).closest('tr').find('.select-parts option:selected').val()){

  

//   var values = $(this).find('.select-parts').map(function() {
//   console.log(this.value);
//    return this.value;
// }).get();


// arr.push(values);


$(".select-parts").each(function(){


  if($(this).val() == 'de'){

    $('#'+idd).find(".working").prop('disabled', true);
    //$('#'+idd).find(".notworking").prop('disabled', true);

  }



   
// Add $(this).val() to your list
//     console.log($(this).val());
      arr.push($(this).val());
       
     
       newArray = arr.filter(function(v){return v!==null});
   console.log(newArray);
 thepast.push($(this).val());
  var hisarr = thepast.filter(function(v){return v!==null});

  history = jQuery.unique( hisarr );
  console.log(history);
});



function checkValue(value,arr){
  var status = 'Not exist';
 
  for(var i=0; i<arr.length; i++){
    var name = arr[i];
    if(name == value){
      status = 'Exist';
      break;
    }
  }

  return status;
}





var i;
 for ( i = 0; i < history.length; ++i) {
 var vh = history[i];
//  // console.log( 'Index : ' + newArray.indexOf(vh) );

console.log(checkValue(vh, newArray));

var check = checkValue(vh, newArray);
   
    if(check == 'Not exist'){

//       console.log(vh + '-'+ selpoint);
  $(".select-parts option[value=" + vh + "]").prop('disabled',false);
     }

 }














var i;

        for ( i = 0; i < newArray.length; ++i) {

var vv = newArray[i];   


  $(".select-parts option[value=" + vv + "]").prop('disabled', function() {
    

      selpoint = this.value;
      console.log(selpoint + '-'+ vv);
  
   return this.value == vv;
  
      });


  } 






  
     
 newArray=[];












 

});

  var i = 0;






$('#addmore2').on('click', function() {

alert('hello');
var previous2 = $("#row"+id+" .select-parts").val();
if(previous2 == 'de'){

          

          
addlimit--;

if(addlimit < 0){

  addlimit = 0;
}

    alert('please fill in line.')
  }else{
    $('#addMore').click();
  }




});

$('#bioscheck').click(function(){

if($("#bioscheck").prop("checked") == true){
  $("#biopass").attr('disabled', true);
  $("#biopass").val('N/A');
}else if($("#bioscheck").prop("checked") == false){
  $("#biopass").attr('disabled', false);
  $("#biopass").val('');
  
};
});






$('input[type="checkbox"]').click(function(){

if($("#expmt").prop("checked") == true){
  $("#prem").attr('disabled', true);
     $("#prem").val('EXEMPT');

    }else{
      $("#prem").attr('disabled', false);
      $("#prem").val(' ');

    }

});
  if($("#prem").val()== ''){
$("#prem").val('EXEMPT');
$("#expmt").prop("checked", true);
}

// if($('parts option:selected').val()=='PC'){


// console.log('pc');


// }


            var ItemArray = [];

       

            $('#country').on('change', function() {
            if($("#country option:selected").text() == 'Wales'){

          //alert('hello');
          $("#expmt").prop("checked", false);
          $("#prem").val(" ");
          $('#premisestuff').show();

            }
            else{
              $("#prem").val("EXEMPT");      
          $('#premisestuff').hide();

            }
          });



  $('.select-parts').on('change', function() {


    arr = [];
 newArray = [];





    var part = $('#row0 #part option:selected').text();
    var partother = $('#row0 #part option:selected').val();
    alert(part);






      if(part == 'PC'|| part == 'ALLINONE PC' || part == 'LAPTOP'|| part == 'TFT'|| part == 'SERVER' || part === 'Network-Switches' || part == 'Apple Phone' || part == 'Smart Phone' || part == 'Apple Tablet' || part == 'Tablet' || part == 'TV'){

         $(this).css('background-color', '#bcdf20');

      }else{
        $(this).css('background-color', '');
      }

      if(part == 'TFT' || part == 'Network-Switches' || part == 'Hard-Drive' || part == 'SERVER'){

        $('#tb tr').find("td:eq(4)").find('.w').hide();
      }else{
        $('#tb tr').find("td:eq(4)").find('.w').show();
      }

      if(part == 'CRT' || part == 'PROJECTOR' || part == 'DESKTOP-PRINTER' || part == 'STANDALONE-PRINTER' || part == 'SMARTBOARD' || partother == 19 || partother == 21 || partother == 23){

          $('#tb tr').find("td:eq(4)").find('.w').hide();
          $('#tb tr').find("td:eq(3)").find('.v').hide();
          }else if(part == 'TFT' || part == 'Network-Switches' || part == 'Hard-Drive' || part == 'SERVER'  || part == 'Apple Phone' || part == 'Smart Phone' || part == 'Apple Tablet' || part == 'Tablet' || part == 'TV'){

$('#tb tr').find("td:eq(4)").find('.w').hide();
}       
          
          
          else{

          $('#tb tr').find("td:eq(4)").find('.w').show();
          $('#tb tr').find("td:eq(3)").find('.v').show();

          }


  });



//   $('#formstuff').submit(function(e) {
//     e.preventDefault();
//     var or = $("#org").val();
//   var emai = $("#email").val();
//   var te = $("#tel").val();
//   var pre = $("#prem").val();
//   var ad1 = $("#add1").val();
//   var ad2 = $("#add2").val();
//   var ad3 = $("#add3").val();
//   var tw = $("#twn").val();
//   var postcod  = $("#postc").val();
//   var contac = $("#cont").val();
//   var contactph = $("#contph").val();
//   var colldateno = $("#coldate").val();
//   var colinstruc = $("#colins").val();
//   var requstco = $("#reqcon").val();
//   var po = $("#pos").val();
//   var sic = $("#sicco").val();

//     $(".error").remove();
 
//     if (or.length < 1) {
//       $('#org').after('<span class="error">This field is required</span>');
//     }
//     if (emai.length < 1) {
//       $('#tw').after('<span class="error">This field is required</span>');
//     }
//     if (te.length < 1) {
//       $('#tel').after('<span class="error">This field is required</span>');
//     }
//     if (ad1.length < 1) {
//       $('#ad1').after('<span class="error">This field is required</span>');
//     }
//     if (postcod.length < 1) {
//       $('#contac').after('<span class="error">This field is required</span>');
//     } else {
//       var regEx = /^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/;
//       var validEmail = regEx.test(email);
//       if (!validEmail) {
//         $('#emai').after('<span class="error">Enter a valid email</span>');
//       }
//     }
//     if (sic.length < 8) {
//       $('#sicco').after('<span class="error">Password must be at least 8 characters long</span>');
//     }
//   });
//   $('form[id="formstuff"]').validate({
//     rules: {
//         Organisation: 'required',
//         country: 'required',
//       user_email: {
//         required: true,
//         email: true,
//       },
//       SICcode: {
//         required: true,
//         minlength: 5,
//       }
//     },
//     messages: {
//         Organisation: 'This field is required',
//         country: 'This field is required',
//       user_email: 'Enter a valid email',
//       SICcode: {
//         minlength: 'SIC-code must be at least 8 characters long'
//       }
//     },
//     submitHandler: function(form) {
//     form.submit();
    
//   }
// });






  $('#additem').on('click', function() {


    var previous3 = $("#row"+id+" .select-parts").val();

if(previous3 == 'de'){

          

          
addlimit--;

if(addlimit < 0){

  addlimit = 0;
}

    alert('please fill in line.')
  }else{
    if(count > 0){
      othercount++;
  val++;
  val++;
 count--;
// alert(val);

othercheck++;

var value =  $('#newitem').val();


  $cont_stat= $('#limit').text(count);


  


$cont_stat.css("font-weight","Bold");

$('.select-parts').append('<option value="'+val+'" id="other'+othercount+'">'+ value +' </option>');

newline = '<option value="'+val+'"  id="other'+othercount+'">'+ value +' </option>'

newline2 = '<option value="'+val+'"  id="other'+othercount+'">'+ value +' </option>'




$("#addMore").click();


$("#row"+id+" .working").prop('disabled', false);
//$("#row"+id+" .notworking").prop('disabled', false);

       

    }if(count <= 0){
    $cont_stat= $('#limit').text("you have gone over your limit");
    }

    $("#newitem").val(" ");
  alert("Your item has been added to item list below.");





  $("#row"+id+" .select-parts").val(val);
  $("#row"+id+" .w").hide();
  $("#row"+id+" .v").hide();


      }
 

  });
  


                   if($("#row"+id+" .working").val() == ''){
          
          $("#row"+id+" .working").val('0');
          
     
 }

            // if($("#row"+id+" .notworking").val() == ''){

//$("#row"+id+" .notworking").val('0');

//}







    $('#addMore').on('click', function() {

      

      addlimit++;

      alert(addlimit);



      if(addlimit >= 19){


        $('#addMore').prop('disabled', true);
        
        $('#addMore2').prop('disabled', true);

        
        alert('Sorry you can not add any more lines.');


      }else{
        ///$('.select-parts').append('<option value="foo" selected="selected">Foo</option>');
       
        $('#addMore').prop('disabled', false);
        
        $('#addMore2').prop('disabled', false);


        var previous = $("#row"+id+" .select-parts").val();
        var workin = $("#row"+id+" .working").val();
       // var notworkin = $("#row"+id+" .notworking").val();

        if(previous == 'de'){

          

          
      addlimit--;

      if(addlimit < 0){

        addlimit = 0;
      }

          alert('please fill in line.')
        }else{


         if($("#row"+id+" .v").prop("checked") == true){
          assettick = 1;
             
               
           }
          else if($("#row"+id+" .v").prop("checked") == false){
            assettick = 0;
           
               
            }

               if($("#row"+id+" .w").prop("checked") == true){
                wipetick = 1;
             
               
           }
          else if($("#row"+id+" .w").prop("checked") == false){
            wipetick = 0;
           
               
            }






//console.log(ItemArray);


       
//pre.push(previous);


//console.log(pre.length);
console.log(pre);

id++;
i++;




// sessName = id;




// ItemArray[0].id.push(previous);
// ItemArray[0].working.push(workin);
// ItemArray[0].notworking.push(notworkin);
// ItemArray[0].asset.push(assettick);
// ItemArray[0].wipe.push(wipetick);



//ItemArray.push({id : previous,working : workin, notworking : notworkin, asset : assettick, wipe : wipetick});

   
              //var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
              //data.find("input").val('');

             var da = $("#tb tr:last").after("<tr id='row"+id+"'>"
             + "<td>"
             + "<div class='control-group'>"
             + "<div class='dropdown'>"
             + "<select name='select-parts' id='part' class='form-control select-parts'>"
             + "<option value='de' selected>Select Part Type</option>"
             +" <option value='3'>PC</option>"
              +" <option value='31'>ALLINONE PC</option>  <option value='5'>LAPTOP</option> <option value='41'>LAPTOP Trolly</option>"
              +"<option value='47'>Smart Phone</option>  <option value='45'>Apple Phone</option>"
              +"   <option value='49'>Apple Tablet</option>"
              +" <option value='51'>Tablet</option>"
              +"  <option value='11'>TFT</option>"
              +" <option value='15'>TV</option>"
               +"<option value='7'>SERVER</option>"
               +"<option value='43'>Network-Switches</option>"
              +"<option value='17'>Hard-Drive</option>"
              +" <option value='29'>SMARTBOARD</option>"
              +"<option value='25'>DESKTOP-PRINTER</option>"
              +"<option value='27'>STANDALONE-PRINTER</option>"
              +"   <option value='9'>PROJECTOR</option>"
              +" <option value='13'>CRT</option>"
              +newline
               +"</select>"
              +" </div>"
               +"</div>"
              +"</div>"
               +"</td>"
             +"<td><input type='number' class='working' min='0' max='5000'  value='0'></td><td hidden><input type='number' class='notworking' min='0' max='5000'  value='0'></td>"
             +"<td><input type='checkbox' class='v' class='group1'></td><td><input type='checkbox' class='w' name='wipe' class='group1'></td>"
             +"<td class='delline'><a href='javascript:void(0);'  class='remove'><span class='glyphicon glyphicon-remove'></span></a>"
+"<span class='tooltiptext2'>remove a line</span></td>"
             +"</tr>")     
             
             



             if(othercheck == 1){

             

         
$("#row"+id+" .working").prop('disabled', false);

//$("#row"+id+" .notworking").prop('disabled', false);

              othercheck = 0;


             }







//              pre.forEach(function(element) {
                 
//               $("#row"+id+"").find(".select-parts option[value='"+element+"']").remove();  
// });


// var partorg = $('#row0 #part option:selected').val();

//  curname = $('#row0 #part option:selected').text();

// cur = partorg;
//              $("#row"+id+" #part option[value="+partorg+"]").each(function() {
//     $(this).remove();
// });


// $("#tb tr").find(".select-parts").on('change', function() {


// });







// if(id > 1){

//   var prev = id -1;

// var partorg = $("#row"+prev+" #part option:selected").val();
//              $("#row"+id+" #part option[value="+partorg+"]").each(function() {
//     $(this).remove();
// });
// }


// var i;
// for (i = 0; i < newArray.length; ++i) {
//  // console.log( index + ": " + value );


// var vv = newArray[i];
//       console.log(vv);
//       console.log(this);
//       $$("#tb tr").find("#row"+id+" .select-parts option[value=" + vv + "]").prop('disabled', true);
// //$(this).find('option').prop('disabled', function() {


//   // return this.value == vv;
  


//       // });


//   } 


// $("#row"+id+" .working").prop('disabled', true);

// $("#row"+id+" .notworking").prop('disabled', true);



 $("#tb tr").find(".select-parts").on('change', function() {

  arr = [];
 newArray = [];


var partex = $("#row"+id+"").find('.select-parts option:selected').text();
var partotherex = $("#row"+id+"").find('.select-parts option:selected').val();
alert(partex);





//   alert('hit');
//    var self = $('#row0 #part option:selected').val();
//     console.log(self);
//  $('#row1 .select-parts').find('option').prop('disabled', function() {
//    console.log(this.value);
//      return this.value == self
//  });





if(partex !== 'de'){
  $("#row"+id+" .working").prop('disabled', false);

//$("#row"+id+" .notworking").prop('disabled', false);
  
}else{

  $("#row"+id+" .working").prop('disabled', true);

//$("#row"+id+" .notworking").prop('disabled', true);

}



  if(partex == 'PC'|| partex == 'ALLINONE PC' || partex == 'LAPTOP'|| partex == 'TFT'|| partex == 'SERVER' || partex == 'Network-Switches' || partex == 'Apple Phone' || partex == 'Smart Phone' || partex == 'Apple Tablet' || partex == 'Tablet' || partex == 'TV'){



     $(this).css('background-color', '#bcdf20');

  }else{
    $(this).css('background-color', '');

  }



if(partex == 'CRT' || partex == 'PROJECTOR' || partex == 'DESKTOP-PRINTER' || partex == 'STANDALONE-PRINTER' || 
partex == 'SMARTBOARD' || partotherex == 19 || partotherex == 21 || partotherex == 23 ){

  $("#row"+id+" .w").hide();
  $("#row"+id+" .v").hide();
}
else if(partex == 'TFT' || partex == 'Network-Switches' || partex == 'Hard-Drive' || partex == 'SERVER' || partex == 'Apple Phone' || partex == 'Smart Phone' || partex == 'Apple Tablet' || partex == 'Tablet' || partex == 'TV'){

$("#row"+id+" .w").hide();
$("#row"+id+" .v").show();
}
else{

  $("#row"+id+" .w").show();
  $("#row"+id+" .v").show();




}
  
  


});


    alert("a new line has been added");

        }

    }




              
     });

 

     





   $('#formstuff').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});


//$("#testbutt").click(function(){

     $('#formstuff').submit(function(e) {

  e.preventDefault();




  //uploadimage();


    


var other1name
    if($('#other1').length){
     other1name = $('#other1').text();

      alert(other1name);
}


var other2name
if($('#other2').length){
     other2name = $('#other2').text();

       alert(other2name);
}


var other3name
if($('#other3').length){
     other3name = $('#other3').text();

      alert(other3name);
}


//var newArray = ItemArray.filter(function(v){return v!==''});



// var newArray = ItemArray.filter(function (el) {
//   return el != null;
// });

console.log(ItemArray);




  var org = $("#org").val();
  var email = $("#email").val();
  var tel = $("#tel").val();
  var prem = $("#prem").val();
  var add1 = $("#add1").val();
  var add2 = $("#add2").val();
  var add3 = $("#add3").val();
  var twn = $("#twn").val();
  var postcode  = $("#postc").val();
  var contact = $("#cont").val();
  var contactphne = $("#contph").val();
  var colldatenote = $("#coldate").val();
  var colinstruct = $("#colins").val();
  var requstcon = $("#reqcon").val();
  var pos = $("#pos").val();
  var sic = $("#sicco").val();
  var biopass = $("#biopass").val();


console.log(biopass);


  
var country = $('#country option:selected').val();






     







 $.each(ItemArray, function( index, value ) {
//   //alert( index + ": " + value );

if(ItemArray.some(arr => arr.working === undefined)){
       // alert("Object found inside the array.");
        //ItemArray.splice(index);
       
    } else{
       // alert("Object not found.");
    }





 });

//console.log(Object.keys(ItemArray[0]));






if($("#expmt").prop("checked") === true){
     is_exempt = 1;
      
        
    }
if($("#expmt").prop("checked") === false){
     is_exempt = 0;

    }


//alert(is_exempt);

 
    $(".error").remove();
    var vali = 1;
    var errcount = 0;


    var cworking = $("#row"+id+" .working").val();

   // var cnotworking  = $("#row"+id+" .notworking").val();

    var oworking = $("#row0 .working").val();

    //var onotworking  = $("#row0 .notworking").val();



  var RegExphne = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;

  var valtel = RegExphne.test(tel);
  var valphone = RegExphne.test(contactphne);

  var regEx = /^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/;
      var validEmail = regEx.test(email);
if($("#confirmed").prop("checked") === false){

    
     $('#confirmed').after('<br/><span class="error" style="color:red">Please Tick box before submitting</span>');
     if(errcount !== 10){
     $('html, body').animate({
        scrollTop: ($('#confirmed').offset().top)
    },500);
     }
     vali = 0;
     errcount++;
 
 
     json = '';

    }
    if (org.length < 3 || org.length > 50) {
       // ItemArray = [];
      
      $('#org').after('<br/><span class="error" style="color:red">This field is required: between 3 - 50 Characters</span>');
      vali = 0;
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#org').offset().top)
    },500);
      }
      errcount++;
      alert(vali);
 
      json = '';
    }

    if (biopass.length < 1) {
       // ItemArray = [];
      $('#bioscheck').after('<br/><span class="error" style="color:red">This field is required</span>');
      vali = 0;
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#bioscheck').offset().top)
    },500);
      }
      errcount++;
      alert(vali);
    
      json = '';
   }
    if (twn.length < 1) {
       // ItemArray = [];
      $('#twn').after('<br/><span class="error" style="color:red">This field is required</span>');
      vali = 0;
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#twn').offset().top)
    },500);
      }
      errcount++;
      alert(vali);
    
      json = '';
   }
    if (tel.length < 10) {
       // ItemArray = [];
      $('#tel').after('<br/><span class="error" style="color:red">This field is required: more than 10 digits</span>');
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#tel').offset().top)
    },500);
      }
      vali = 0;
      errcount++;
      alert(vali);
    
      json = '';
    }
    if (!valtel) {
      //  ItemArray = [];
      $('#tel').after('<br/><span class="error" style="color:red">This field is required: Please use a valid number</span>');
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#tel').offset().top)
    },500);
      }
      vali = 0;
      errcount++;
      alert(vali);
  
      json = '';
    }
    if (!valphone) {
      // ItemArray = [];
      $('#contph').after('<br/><span class="error" style="color:red">This field is required: Please use a valid number</span>');
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#contph').offset().top)
    },500);
      }

      vali = 0;
      errcount++;
      alert(vali);
 
      json = '';
    }
    if (add1.length < 5) {
       // ItemArray = [];
      $('#add1').after('<br/><span class="error" style="color:red">This field is required</span>');
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#add1').offset().top)
    },500);
      }
      vali = 0;
      errcount++;
      alert(vali);
    
      json = '';
    }
    if (postcode.length < 6 || postcode.length > 10) {
       // ItemArray = [];
      $('#postc').after('<br/><span class="error" style="color:red">This field is required: between 6 - 10 Characters </span>');
      if(errcount !== 10){
      $('html, body').animate({
        scrollTop: ($('#postc').offset().top)
    },500);
      }
      vali = 0;
      errcount++;
   
      json = '';
 
    } 
    else {
      var regEx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      var validEmail = regEx.test(email);
      if (!validEmail) {
        errcount++;
        //ItemArray = [];
        $('#email').after('<br/><span class="error" style="color:red">Enter a valid email</span>');
        if(errcount !== 10){
        $('html, body').animate({
        scrollTop: ($('#email').offset().top)
    },500);
        }
        vali = 0;
        ///ItemArray.pop();
        json = '';
      }
    }
    
    if (sic.length < 5) {
        //ItemArray = [];
      $('#sicco').after('<br/><span class="error" style="color:red">SICcode must be 5 numbers long</span>');
      vali = 0;
      alert(vali);
      //ItemArray.pop();
      json = '';
    }


    if (cworking == 0  || oworking == 0 ) {
        //ItemArray = [];
    //  $('#addmore2').after('<br/><br/><span class="error" style="color:red">there cannot be empty values</span>');
      errcount++;
      if(errcount !== 10){
        $('html, body').animate({
        scrollTop: ($('#addmore2').offset().top)
    },500);
        }
      vali = 0;
      alert(vali);
      //ItemArray.pop();
      json = '';
    }


   console.log(errcount);
    
    if(errcount == 10){

          $('html, body').animate({
        scrollTop: ($('.header').offset().top)
    },500);
   

         errcount = 0;
         vali = 0;
         json = '';

      }

    //console.log(json);

 


    if(vali  == 0){

      deluploadimage();


           
    $('#tb tr').each(function() {
      
    var part = $(this).find(".select-parts option:selected").val();   
    var working = $(this).find(".working").val();  
    //var notworking = $(this).find(".notworking").val();   
    var f_workin = 0;
    var f_assettick = 0;


      
   
  


    if(working == 0 ){


             //ItemArray = [];
      $('#addmore2').after('<br/><br/><span class="error" style="color:red">there cannot be empty values</span>');
      errcount++;
      if(errcount !== 10){
        $('html, body').animate({
        scrollTop: ($('#addmore2 .error').offset().top)
    },500);
        }
      vali = 0;
      alert(vali);
      //ItemArray.pop();
      json = '';

    }


    if($(this).find(".v").prop("checked") == true){

// if ( $( "#v" ).length ) {
 f_assettick = 1;


  }
 else if($(this).find(".v").prop("checked") == false){

 //  if ( $( "#v" ).length ) {
   f_assettick = 0;

 //  }
  
      
   }

      if($(this).find(".w").prop("checked") == true){

     //  if ( $( "#v" ).length ) {
       f_wipetick = 1;

      // }
    
      
  }
 else if($(this).find(".w").prop("checked") == false){
 //  if ( $( "#v" ).length ) {
   f_wipetick = 0;

  // }
  
      
   }
       

        //  ItemArray = [];
        if(working != undefined){
       
          ItemArray.push({id : part, working : working,  asset : f_assettick, wipe : f_wipetick});
       
     
        }




       
           
       
 });
 //ItemArray.splice(0, 1);

// alert(ItemArray);

var json = JSON.stringify(ItemArray);  

//var jsonclean = json.replace(",{}", " ");

//jsonsuperclean = jsonclean.replace(',{"asset":0,"wipe":0}', ' ');


 console.log(ItemArray); 
    
    console.log(json); 

    ItemArray = [];
     


    }else if(vali  == 1 && check == 0){

  
        e.preventDefault();
        json = '';
        $('#myModal').css( "display", "block" );

    }else if (vali  == 1 && check == 1){

      $('#tb tr').each(function() {
        
    var part = $(this).find(".select-parts option:selected").val();    
    var working = $(this).find(".working").val();  
    //var notworking = $(this).find(".notworking").val();   
    var f_workin = 0;
    var f_assettick = 0;

    console.log(this);
   
   
    //alert(part);


    if($(this).find(".v").prop("checked") == true){

// if ( $( "#v" ).length ) {
 f_assettick = 1;


  }
 else if($(this).find(".v").prop("checked") == false){

 //  if ( $( "#v" ).length ) {
   f_assettick = 0;

 //  }

   }

      if($(this).find(".w").prop("checked") == true){

     //  if ( $( "#v" ).length ) {
       f_wipetick = 1;

      // }
    
      
  }else if($(this).find(".w").prop("checked") == false){
 //  if ( $( "#v" ).length ) {
   f_wipetick = 0;

  // }
  
   }
       

        //  ItemArray = [];
        if(working != undefined){
       
          ItemArray.push({id : part, working : working, asset : f_assettick, wipe : f_wipetick});
       
     
        }




       
           
       
 });
 //ItemArray.splice(0, 1);

 //alert(ItemArray);

var json = JSON.stringify(ItemArray);  

//var jsonclean = json.replace(",{}", " ");

//jsonsuperclean = jsonclean.replace(',{"asset":0,"wipe":0}', ' ');


 console.log(ItemArray); 
    
    console.log(json); 

    





  
  if(vali == 1){
  


  $.ajax({
type: "POST",
url: "updatetables.php",
data: {
 org : org,
 email : email,
 tel : tel,
 prem : prem, 
 add1 : add1,
 add2 : add2,
 add3 : add3,
 twn : twn,
 postcode : postcode,
 contact : contact,
 contactphne : contactphne,
 colldatenote : colldatenote,
 colinstruct : colinstruct,
 requstcon : requstcon,
 pos : pos,
 biopass : biopass,
 json : json,
 other1name : other1name,
 other2name : other2name,
 other3name : other3name,
 country : country,
 //sic : sic,
 is_exempt : is_exempt

},
dataType: "json",
success:function(data) {
 //alert(data);
 alert("Thank for you submission we have sent you an email");
 
 }
});
setTimeout(function() {
  window.location.replace("thankyou.php");
}, 2000);


}

    }  


});






     
     $(document).on('click', '.remove', function() {
      var working = $(this).find(".working").val();
     // var working = $(this).find(".notworking").val();
      var newrow = $(this).find(".select-parts").val();
         var trIndex = $(this).closest("tr").index();
      //   alert(newrow);
            if(trIndex>-1) {
            // $(this).closest("tr").remove();
             $(this).closest("tr").find("td:eq(4)").remove();
             $(this).closest("tr").find("td:eq(3)").remove();
             $(this).closest("tr").find(".v").remove();
             $(this).closest("tr").find(".w").remove();
             $(this).closest("tr").remove();

              //f_assettick = 0;
              // f_wipetick = 0;



          
             


          
           } else {
             alert("Sorry!! Can't remove first row!");
         
           }
      });


      function uploadimage(){

             var uploadcheck = 0;
 

            var fd = new FormData();
            jQuery.each(jQuery('input[type=file]'), function(i, value) 
            {
             
              filestuff = $(this).val();
            // alert(filestuff);

             if(filestuff == ''){
                alert("no files");
           
                uploadcheck = 1;

                filecheck = uploadcheck;

             }else{
                uploadcheck = 0;

                filecheck = uploadcheck;
            

          
            
              var filetypestr = filestuff.substr((filestuff.lastIndexOf('.') +1));

              if(filetypestr == ' '){

              }else{
                     console.log(filetypestr);
              if(filetypestr === 'jpg' || filetypestr === 'png' || filetypestr === 'jpeg'){
               filesize = value.files[0].size;
              console.log(filesize);

                  fd.append('inputfile['+i+']', value.files[0]);
                  console.log(fd.get("inputfile"));

              }else if(filesize > 1585152){
                alert("sorry the file named:"+filestuff+" is to large");
              }else{
                  alert("sorry one or more of your files are not supported");
               }
              }
          }
           
            });
      // var fd1 = new FormData();
      // var files1 = $('#file1')[0].files1[0];
      // fd1.append('inputfile1',files1);

      // var fd2 = new FormData();
      // var files2 = $('#file2')[0].files2[0];
      // fd2.append('inputfile2',files2);

if(uploadcheck == 0){
  $.ajax({
          url: 'upload.php',
          type: 'POST',
          data: fd,
          contentType: false,       
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false
          success: function(response){
              if(response != 0){
                 alert('uploaded');
              }else{
                  alert('file not uploaded');
              }
          },
      });
}


}

function deluploadimage(){


var del = 1;
// var fd1 = new FormData();



// var fd2 = new FormData();
// var files2 = $('#file2')[0].files2[0];
var fd = new FormData();
            jQuery.each(jQuery('input[type=file]'), function(i, value) 
            {
              fd.append('inputfile['+i+']', value.files[0]);

                console.log(fd.get("inputfile"));
            });
            fd.append('del',1);



$.ajax({
          url: 'upload.php',
          type: 'POST',
          data: fd,
          contentType: false,       
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false
          success: function(response){
              if(response != 0){
                 alert('uploaded');
              }else{
                  alert('file not uploaded');
              }
          },
      });

}

 

});      
</script>

    
</body>

</html>