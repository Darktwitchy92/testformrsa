<?php
/*----------------------------------------------------------------------------------------------------
Change Log
Date			tag						Ticket				By						Description
------------------------------------------------------------------------------------------------------
06/09/18	Created											Alex.Smith		Created
07/09/18	tidy                        Neil.Baker    Tidy HTML and remove incorrect tags, remove duplicate includes for DB, move DB include to top of file
------------------------------------------------------------------------------------------------------*/

include_once('db.php');


if(!isset($_SESSION)) {
  session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/inc/js/jquery-3.3.1.js"></script>
    <script src="/inc/js/bootstrap.js"></script>

    <script src="/inc/js/popper.js"></script>

    <link href="/inc/css/bootstrap.min.css" rel="stylesheet">
    <link href="/inc/css/imgupload.css" rel="stylesheet">
  


    <title>Request Details</title>
</head>

<style>
  th {
    background:#EAEAEA;
  }
  textarea {
  width: 300px;
  height: 150px;
}
#emailcon {

  margin-right: 240px;
}


</style>
<body>
<ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link" href="/RS/booking/">Booked Collection</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/RS/arc/">Collected</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/RS/RGR/">Recycling Goods Receipting </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/RS/companynote/">Company Notes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/RS/rebatepage/">Rebates</a>
      </li>
     
    </ul>

    <hr>
    <h1>Request Details</h1>
    <h3> <?php echo $_GET['rowid']; ?> </h3>

    <div class= "row">

    <?php include_once("detaildata.php");?>

    <!--</table>-->

      <div class="col-xl-4">
        <?php


$postcode = '';

$stmtnote = $conn->prepare($sqlnotes);
if (!$stmtnote) {
  echo "\nPDO::errorInfo():\n";
  print_r($conn->errorInfo());
  die();
}
$stmtnote->execute();


        echo "<table cellpadding='1'cellspacing ='1' class='table'>";


        $sqlord = "

        exec P_Getgrennoakord '".$_GET['rowid']."'
        ";
          
        
        $stmtord = $conn->prepare($sqlord);
        if (!$stmtord) {
          echo "\nPDO::errorInfo():\n";
          print_r($conn->errorInfo());
          die();
        }
        $stmtord->execute();
        $dataord = $stmtord->fetch(PDO::FETCH_ASSOC);

        echo "        <tr>
        <th >GreenOak ORD</th>
        <td><input type='textbox' id='ord_name' class='group1' value='". $dataord['ord'] ."'/disabled></td>
      </tr>";

        // while($row = sqlsrv_fetch_array($stmt)){
        foreach($data as $row) {

          
          $_SESSION['owner'] = $row['ownerreq'];


        $geprstring = '';

          if($row['gdprcon'] == 1){
            $geprstring = 'Yes';
          }else{
            $geprstring = 'No';
          }
    
         
          echo "



          <tr>
          <th >GDPR</th>
          <td>        
            <select id='gdpr-select'  id='gdprup' class='group1'>
          <option value='".$row['gdprcon']."' selected>".$geprstring."</option>
          <option value='0' >No</option>
          <option value='1' >Yes</option>
          </select>
          <input type='textbox' id='gdprs' value='".  $geprstring  ."'/disabled>
          </td>
        </tr>
          <tr>
            <th >Name</th>
            <td><input type='textbox' id='name_ed' class='group1' value='". $row['name'] ."'/disabled></td>
          </tr>
          <tr>
            <th> Requested By  </th>
            <td>". $row['contact'] ."</td>
          </tr>
          <tr>
            <th>Email Address</th>
            <td><input type='textbox' id='email_ed' class='group1' value='". $row['email'] ."'/disabled></td>
          </tr>
          <tr>
            <th> customer Telephone  </th>
            <td><input type='textbox' id='telcust_ed' class='group1' value='". $row['cphone'] ."'/disabled></td>
          </tr>
          <tr>
          <th> Contact Telephone  </th>
          <td><input type='textbox' id='tel_ed' class='group1' value='". $row['tel'] ."'/disabled></td>
        </tr>
          <tr>
            <th>Position </th>
            <td><input type='textbox' id='pos_ed' class='group1' value='". $row['position'] ."'/disabled></td>
          </tr>
          <tr>
            <th> Address1 </th>
            <td><input type='textbox' id='add1_ed' class='group1' value='". $row['address1'] ."'/disabled></td>
          </tr>
          <tr>
            <th> Address2 </th>
            <td><input type='textbox' id='add2_ed' class='group1' value='". $row['address2'] ."'/disabled></td>
          </tr>
          <tr>
            <th> Address3 </th>
            <td><input type='textbox' id='add3_ed' class='group1' value='". $row['address3'] ."'/disabled></td>
          </tr>
          <tr>
          <th> Postcode </th>
          <td><input type='textbox' id='post_ed' class='group1' value='". $row['postcode'] ."'/disabled></td>
        </tr>
        <tr>
        <th> Town </th>
        <td><input type='textbox' id='twn_ed' class='group1' value='". $row['twn'] ."'/disabled></td>
      </tr>
      <tr>
      <th> Owner </th>
      <td><input type='textbox' id='reqown_ed' class='group1' value='".$row['ownerreq']."'/disabled></td>
    </tr>
    <tr>
    <th> Previous Date </th>
    <td><input type='textbox' id='reqprev_ed' class='group1' value='".$row['prev']."'/disabled></td>
  </tr>
    <tr>
    <th> Vehicle </th>
    <td><input type='textbox' id='dtype_ed' class='group1' value='".$row['typ']."'/disabled></td>
  </tr>

     
          ";

         
        }





        $stmtnote = $conn->prepare($sqlnotes);
        if (!$stmtnote) {
          echo "\nPDO::errorInfo():\n";
          print_r($conn->errorInfo());
          die();
        }
        $stmtnote->execute();

        $count = $stmtnote->rowCount();

//echo $count;

        
        $datanote = $stmtnote->fetchAll(PDO::FETCH_ASSOC);
        foreach($datanote as $row) {


        echo " <tr hidden>
        <th> notes</th>
        <td><textarea>  ".$row['notes']." </textarea></td>
        </tr>
        <tr hidden>
                <th> GDPR Contract </th>
                <td> ".$row['gdpr']." </td>
        </tr>
        <tr hidden>

        <th> Company AMR status </th>
        <td> ".$row['amrc']." </td>
</tr>
        
        </table>";
        }

  


    

     
?>


        <script type="text/javascript">
          $(document).ready(function (e) {


       


            $('#newlinedata').hide();


            $("#addlineBTN").click(function(){ 

              $('#newlinedata').show();
              $('#addlineBTN').hide();

            });



            $("#newlinedata #part-select").on('change', function() {


              alert('hello');

              var partselected = $('#part-select option:selected').val();

      if(partselected == 19 || partselected == 21 || partselected == 23){

           $('#othername').show();


                         }
              });

            
            $("#subline").click(function(){ 

              $('#newlinedata').hide();
              $('#addlineBTN').show();  
              
               var workingqtynew = $('#newwork').val();
              var notworkingqtynew = $('#newnotwork').val();

              var reqidn =  <?php echo $_GET['rowid']; ?> ;
              var partselected = $('#part-select option:selected').val();

              var othername = '';

              if($('#othername').length > 0){

                othername = $('#othername').val();

              }

              if( $('#newwork').val() == ''){

                workingqtynew = 0;
              }


              
              if( $('#newnotwork').val() == ''){

                notworkingqtynew = 0;
                }

             

      

            
           

            //  alert(workingqtynew);

              $.ajax({
                url: "/RS/Addnewline/", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                  partselected : partselected,
                  workingqtynew : workingqtynew,
                  notworkingqtynew : notworkingqtynew,
                  reqidn : reqidn,
                  othername : othername
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                      // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
            
               alert('success');
               alert(data);
               location.reload(true);
                  // $("#image_preview").html(data);
                }
              });

            });


        

            $("#ntb").on('click', '.remove', function() {
      var working = $(this).find(".working").val();
      var working = $(this).find(".notworkin").val();
    
         var trIndex = $(this).closest("tr").index();
      //   alert(newrow);
            if(trIndex>-1) {
            var partid = $(this).closest("tr").find(".idp").val();
             var workingqty = $(this).closest("tr").find(".workin").val();
              var notworkingqty =  $(this).closest("tr").find(".notworkin ").val();
              var requestid = <?php echo $_GET['rowid']; ?>;
            // $(this).closest("tr").remove();
  

            $.ajax({
                url: "/RS/Delline/", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                  partid : partid,
                  workingqty : workingqty,
                  notworkingqty : notworkingqty,
                  requestid : requestid
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                      // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
            
               alert('success');
               alert(data);
               location.reload(true);
                  // $("#image_preview").html(data);
                }
              });
             


          
           } 
      });




            $('#loading').hide();
            $("#uploadimage").on('submit',(function(e) {
              e.preventDefault();

              $("#message").empty();
              $('#loading').show();

              $.ajax({
                url: "/RECbooking/php_upload.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
                  $('#loading').hide();
                  $("#message").html(data);
                  // $("#image_preview").html(data);
                }
              });
            }));

            // Function to preview image after validation
            $(function() {
              $("file").change(function() {
                $("message").empty(); // To remove the previous error message
                var file = this.files[0];
                var imagefile = file.type;
                var match= ["image/jpeg","image/png","image/jpg"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
                  $('previewing').attr('src','noimage.png');
                  $("message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                  return false;

                } else {
                  var reader = new FileReader();
                  reader.onload = imageIsLoaded;
                  reader.readAsDataURL(this.files[0]);
                }
              });
            });

            function imageIsLoaded(e) {
              $("file").css("color","green");
              $('image_preview').css("display", "block");
              $('previewing').attr('src', e.target.result);
              $('previewing').attr('width', '250px');
              $('previewing').attr('height', '230px');
            };
          });

          <?php
          $sqly = 'SELECT filename AS fi FROM customerPics WHERE Request_ID ='.$_GET['rowid'];
          $stmt6 = $conn->prepare($sqly);
          if( $stmt6 === false) {
              die("SQL query failed: ".$sqly);
          }
          $stmt6->execute();
          $data = $stmt6->fetchAll(PDO::FETCH_ASSOC);

          $images  = array();
          foreach($data as $row) {
            $images[] = $row['fi'];
          }

          $fn = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/img.txt","a+");
          fwrite($fn,print_r( $images, true)."\n");
          fclose($fn);
          ?>

          var index = 0;

          function changeing(){

            var imagearray = ["<?php echo implode('","',$images);?>"];
            var myimage = document.getElementById('kool');
            // alert(imagearray[index]);
            myimage.setAttribute("src",imagearray[index]);
            index++;
            console.log( index);
            console.log( imagearray);
            if(index > imagearray.length-1){
              {index = 0;}
            }
            console.log( index);
            console.log( index.length);
            console.log( imagearray);
            console.log( imagearray.length);
          }
        </script>
      </div>

      

      <h1>More details</h1>
   

      <?php
        $DEADtime = '';


      require('apsform.php');

      ?>

<?php  
//require("amrsqlquery.php");
?>


<script type="text/javascript">
    
   $(document).ready(function(){


    var gdprselect = $('#gdpr-select option:selected').val();

    if (gdprselect == 0){

      $('#bookingdate').prop('disabled', true);
      $('#subdateform').prop('disabled', true);


    }else{

      $('#bookingdate').prop('disabled', false);
      $('#subdateform').prop('disabled', false);
    }


    $('#ntb tbody tr').each(function() {

var part = $(this).find(".partis").text();  

//alert(part);




if(part == 'CRT' || part == 'Projector' || part == 'DesktopPrinter' || part == 'Standalone_Printer' || part == 'Other1' || part == 'Other2' || part == 'Other3' ||
part == 'SmartBoard'){

  $(this).find("#w").hide();
  $(this).find("#v").hide();
}
else if(part == 'TFT' || part == 'Switches' || part == 'Harddrive' || part == 'Server' || part == 'Apple Phone' || part == 'Smart Phone' || part == 'Apple Tablet' || part == 'Tablet' || part == 'TV'){

  $(this).find("#w").hide();
  $(this).find("#v").show();
}
else{

  $(this).find("#w").show();
  $(this).find("#v").show();




}

});

    $("#subdateform").submit(function(e) {
    e.preventDefault();
});


    $("#btnstats").click(function(){
         
         alert("hello");
      var bokstat = $("#bookingstat option:selected").val();
      var prevnote = $("#boknote").val();
      var req = $("#reqid").val();
      alert(bokstat);
      alert(req);
      alert(prevnote);
      

      $.ajax({
                url: "/RS/bookstat/", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                  bokstat : bokstat,
                  req : req,
                  prevnote : prevnote
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                      // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
            
               alert('success');
               alert(data);
               location.reload(true);
                  // $("#image_preview").html(data);
                }
              });



    });



    /*if($("#lorrytttt").val() != ""){

         $("#lorrytype").hide();
         $('#emailcon').prop('disabled', true);

    }else{

      $("#lorrytype").show();
         $('#emailcon').prop('disabled', false);

    }*/

    $("#amrupdate").click(function(){

      var ord = <?php  if(isset($_SESSION['ord'])){
        echo $_SESSION['ord'];
      }

          echo '0';
        

       ?>;
      var amrs = $('#upamr').val();
      var amrcomp = $('#amr').val();
	        var isrebate = $('#isre').val();
          

      $.ajax({
                url: "/RS/amrupdate/", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
             ord : ord,
             amrs : amrs,
             amrcomp :amrcomp,
			       isrebate : isrebate
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                      // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
            
               alert('success');
               alert(data);
                  // $("#image_preview").html(data);
                }
              });

   
    });

 
    $("#emailcon").click(function(){

      
	
	alert("hello");

var reqid = $("#reqid").val();
      var emailaddress = $("#email_ed").val();
  var loorytype = $("#bookedinfo #lorrytype option:selected").val();
  var date = $('#tbdtl #prodate').html();
  var owner = $('#reqown_ed').val();
  var deadlinetome = '<?php     
         $cenvertedTime = date('Y-m-d H:i:s',strtotime('-5 day -12 hour',strtotime($row['CollectionDate'])));
         
         echo $cenvertedTime?>' ;


  var d = new Date(date);

  if(owner === ''){

    owner = 'alex.smith';
  }

  

  

 alert(deadlinetome);

  alert(d);

  //var date = $('#tbdtl #prodate').val($.datepicker.formatDate('dd M yy', new Date()));



         //alert(loorytype);
         alert(reqid);
         alert(date);


    
    alert(emailaddress);
    alert(loorytype);

              $.ajax({
                url: "/RS/emailconf", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                  loorytype : loorytype,
                  emailaddress : emailaddress,
                  reqid : reqid,
                  date : date,
                  owner : owner,
                  deadlinetome : deadlinetome
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                      // To send DOMDocument or non processed data file it is set to false

                success: function(data){   // A function to be called if request succeeds
            
               alert('success');
               alert(data);
               location.reload(true);
                  // $("#image_preview").html(data);
                }
              });


              //  if($('#lorry').prop("checked") == true){



              
              //  });

    });


   });


   </script>
  
  

<!-------------------------------Upload images----------------------------------------------->


      <div class="stuff">
        <hr>
        <form id="uploadimage" name="uploadimage" action="" method="post" enctype="multipart/form-data">

        

          <?php
          $sqly = 'SELECT filename AS fi FROM customerPics WHERE Request_ID ='.$_GET['rowid'];
          $stmt6 = $conn->prepare($sqly);
          if( $stmt6 === false) {
              die("SQL query failed: ".$sqly);
          }
          $stmt6->execute();
          $data = $stmt6->fetch(PDO::FETCH_ASSOC);
		  
		 // print_r($stmt6->errorInfo());

          echo "<div id='image_preview'><img id='kool' src='".$data['fi']."' alt='' width='250' height='200'></div>"
          ?>

          <hr id="line" name="line">

          <div id="selectImage" name="selectImage">
            <label>Select Your Image</label><br/>
            <input type="file" name="file" id="file" required />
            <input type="submit" value="Upload" class="submit" />
          </div>
        </form>
      </div>

      <h4 id='loading' >loading..</h4>

      <div id="message" name="message"></div>
    </div>


    <button onclick="changeing()">next</button>

    <div class="col-xl-4">

      <?php




$stmttotal = $conn->prepare($totalswu);
if (!$stmttotal) {
  echo "\nPDO::errorInfo():\n";
  print_r($conn->errorInfo());
  die();
}
$stmttotal->execute();

$datar = $stmttotal->fetchAll(PDO::FETCH_ASSOC);

///// end of image upload///



////// start of bottom feilds /////

      echo "<table id='tbdtl'>";
        // while($row = sqlsrv_fetch_array($stmt)){
        foreach($datar as $roww) {
          echo "
          <tr>
            <th>Est Units</th>
            <td>". $roww['totalunits'] ."</td>
          </tr>
          <tr>
            <th>Est Weight</th>
            <td>". $roww['totalweight'] ."</td>
          </tr>
";
        }

        $stmtrest = $conn->prepare($sql);
        if( $stmtrest === false) {
            die("SQL query failed: ".$sql);
        }
        $stmtrest->execute();
        $datarest = $stmtrest->fetchAll(PDO::FETCH_ASSOC);

        foreach($datarest as $row) {



          if(isset($row['ProposedDate'])){

             $timepro = date("d/m/y H:i:s",strtotime($row['ProposedDate']));
             $_SESSION['protime'] =  $timepro;
          }else{

            $timepro = '';
            $_SESSION['protime'] =  $timepro;
          }

          if(isset($row['survdue'])){

            $DEADtime = date("d-m-y H:i:s",strtotime($row['survdue']));
         }else{

           $DEADtime = '';
         }


         $_SESSION['newdeadline'] = $DEADtime;

         

        

 
        echo"
            <tr>
            <th> Approved  </th>
            <td><input type='textbox' id='app_ed' class='group1' value='". $row['approved'] ."'/disabled></td>
          </tr>
          <tr>
          <th> Process  </th>
          <td><input type='textbox' id='pro_ed' class='group1' value='". $row['process'] ."'/disabled></td>
        </tr>
          <tr>
            <th> BIOS Password  </th>
            <td><input type='textbox' id='bio_ed' class='group1' value='". $row['BIOS_Password'] ."'/disabled></td>
          </tr>
          <tr>
            <th>Collection Instruction</th>
            <td><input type='textbox' id='collinstr_ed' class='group1' value='". $row['CollectionInstruction'] ."'/disabled></td>
          </tr>
          <tr>
            <th> Request Date  </th>
            <td><input type='textbox' id='coldate_ed' class='group1' value='". $row['CollectionDate'] ."'disabled></td>
          </tr>
          <tr>
            <th>Proposed Date</th>
            <td id='prodate'>". $timepro ."</td>
          </tr>

          <tr hidden>
          <th>Survey Deadline</th>
          <td> <input type='textbox'id='survdead' class='group1' value='".$DEADtime."' disabled></td>
        </tr>

          <tr>
            <th> Request Done Date  </th>
            <td>". $row['RequestDoneDate'] ."</p></td>
          </tr>
          <tr>
            <th> Update note  </th>
            <td><input type='textbox' id='updatenote_ed' class='group1' value='". $row['Updatenote'] ."'disabled></td>
          </tr>
          <tr HIDDEN>
          <th> Request Stat note  </th>
          <td><input type='textbox' id='reqstatenote_ed' class='group1' value='". $row['statnote'] ."'disabled></td>
        </tr>
          <tr>
            <th>Request Updated By</th>
            <td><input type='textbox' id='reqby_ed' class='group1' value='". $row['RequestUpdatedBy'] ."'/disabled></td>
          </tr>

          <br>
          
          ";
        }

        $booksql = "SELECT RIGHT(ORD, 7) AS ordsa FROM Booked_Collections WHERE RequestID LIKE '".$justID."'";
        $stmtbook = $conn->prepare($booksql);

        if( $stmtbook === false) {
            die("SQL query failed: ".$booksql);
        }

        $stmtbook->execute();
        $r = $stmtbook->fetch(PDO::FETCH_ASSOC);

        $_SESSION['ordnum'] =   $dataord['ord'];

         $trimo = substr($dataord['ord'], 4);
         

        $_SESSION['ord'] = $trimo;

      

        // include_once('dbgreen.php'); // Now part of db.php

        $booksql2 = "SELECT
          [Recycling Account Manager] AS ram,
          [Shared With] AS sw,
          Dept AS dep,
          [Account Manager] AS am
        FROM
          Collections_Log
        WHERE
          ordernum LIKE '". $trimo ."'

          SELECT RIGHT(ORD, 7) AS orr FROM Request JOIN Collections_Log ON RIGHT(ORD, 7) = OrderNum
        ";

        $stmtbook2 = $conn->prepare($booksql2);

        if( $stmtbook2 === false) {
            die("SQL query failed: ".$booksql2);
        }

        $stmtbook2->execute();
        $ec = $stmtbook2->fetch(PDO::FETCH_ASSOC);

        // $stmtbook2 = sqlsrv_query( $conn, $booksql2);
        // if( $stmtbook2 === false) {
        //     die( print_r( sqlsrv_errors(), true) );
        // }
        // $ec = sqlsrv_fetch_array($stmtbook2);

        $sqlgreen = "SELECT
          ConsignmentNumber AS consign
        FROM
          [dbo].Delivery AS d
        JOIN
          SalesOrders AS s ON d.SalesOrderID = s.SalesOrderID
        WHERE
          SalesOrderNumber LIKE '%".$r['ordsa']."'";

        $stmtgreen = $conn2->prepare($sqlgreen);

        $stmtgreen->execute();

        if( $stmtgreen === false) {
            die("SQL query failed: ".$sqlgreen);
        }

        if(isset($r['orr'])){
          echo "<p>allowed</p>";
        }else{
          echo "<p>Needs to be done first. make changes in achive.</p>";
        }

        $rg = $stmtgreen->fetch(PDO::FETCH_ASSOC);

        echo "
        <tr>
          <th>Consignment Number</th>
          <td><input type='textbox' id='consi' class='group1' value='".$rg['consign']."/EXEMPT'/disabled></td>
        </tr>
        <tr hidden>
           <th> Recycling Account Manager </th>
           <td><input type='textbox' id='conram' class='group1' value='".$ec['ram']."'/disabled></td>
        </tr>
        <tr hidden>
          <th>Shared With</th>
          <td><input type='textbox' id='consw' class='group1' value='".$ec['sw']."'/disabled></td>
        </tr>
        <tr hidden>
          <th> Dept </th>
          <td><input type='textbox' id='condep' class='group1' value='".$ec['dep']."'/disabled></td>
        </tr>
        <tr hidden>
          <th> Account Manager </th>
          <td><input type='textbox' id='conam' class='group1' value='".$ec['am']."'/disabled></td>
        </tr>
      </table>
      <br>
   
      ";

      $fn = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/collectionlog.txt","a+");
      fwrite($fn,print_r($booksql, true)."\n");
      fclose($fn);
      $_SESSION['id'] = $_GET['rowid'];
     
    ?>

    </div>
    <div id = 'button'>
      <button type='button' id='buttonID' class='btneddy btn-primary'>edit </button>
      <button type='button' id='buttondoneID' class='btnbuttondoneID btn btn-success'>Done </button>
    </div>
    <br/>

    <div class="col-xl-4">
      <div class="form-group">
        <form method='get'action="/RS/detialdoc" id="form1">
      <input hidden type="text" name="rowid" id="reqid" value = "<?php echo $_GET['rowid']; ?>">
          <div class="col-sm-10">
          <label for="pd"><b>Proposed Date:</b></label>
            <input type="date" class="form-control" placeholder="Enter Date" name="pd" value="" class="date" id='bookingdate'>
           
            <button type="submit" form="form1" value="Submit" name="btn" id='subdateform' data-toggle="modal" data-target="#myModal">Submit</button>
          </div>
        </form>
      <div class="col-xl-4">
        <div class="form-group">
   

        <!-------Ordernumber Checker ------->
          <form hidden method='get'target="_blank" action="/RS/Ordcheck" id="Checkform">
          <label > Ordernumber Checker </label>
            <input type="text"   placeholder="e.g 6019328"  id="ordernumval" name="ordernumval"  value="">
            <button type="submit" value="Submit" id="ordercheck">Check</button>
          </form>
          </div>
          <br>
          <br>
          <br>
          <?php

          if(isset($_GET['btn'])){
            $test = strtotime(str_replace('/', '-', $_GET['pd']));
            $timin =  date("Y-m-d",$test);
          }if(!isset($_GET['pd'])){

          }else{

            //echo "Now in Booking";

            $test = strtotime(str_replace('/', '-', $_GET['pd']));

            $timin =  date("Y-m-d",$test);

            $sqlUpdate = "UPDATE
              request_test2
            SET
            collection_date ='".$timin."',
            been_collected = 1
            WHERE
              Request_ID LIKE ".$_GET["rowid"];

            $datechange = 0;

            $stmtp = $conn->prepare($sqlUpdate);
            $stmtp->execute();


$CHECKIFTHERESQL = "
set nocount on
declare @test varchar(max)
declare @name varchar(50)
set @test = '".$_GET["rowid"]."'
set @name = '".$row['name']."'
select  isnull((case when Customer like @name then 'There all ready' else 'new record' end),'new rec') as Checker from Booked_Collections where RequestID like @test
group by Customer

";


   $st = $conn->prepare($CHECKIFTHERESQL);
            $st->execute();
            $r = $st->fetch(PDO::FETCH_ASSOC);

            $f = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/testrecsql.txt","a+");
            fwrite($f,$r['Checker'] ."\n");
            fclose($f);

            $f = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/testrecsql.txt","a+");
            fwrite($f,$CHECKIFTHERESQL ."\n");
            fclose($f);
           
            //var_dump($CHECKIFTHERESQL);

             $date = date_create($time);
            date_add($date, date_interval_create_from_date_string('4 days'));
            $datediff = date_format($date, 'Y/m/d H:i:s');
   
            $cenvertedTime = date('Y-m-d H:i:s',strtotime('+16 hour',strtotime($datediff)));
            
        
           

            if($r['Checker'] !== 'There all ready') {

              $userint = get_current_user();

              $result = strtoupper(substr($userint, 0, 2));


              $_SESSION['by'] =  $result;

            
            $bookrecord = "

            

    

            insert into Booked_Collections( RequestID, Customer, Email, Contact, Phone,
             Town, PostCode, Prefix, Area1, [Est. Weight],[Est Total Units], [Submitted Date], [Booked Collection Date], [is_canceled], booking_status, [sent by], owner, [deliveryType], prev_date, ORD, [Job notes], [Access Notes], [APS notes], A, P)
            
            values('".$_GET["rowid"]."', '". $row['name'] ."', '".$row['email']."', '".$row['contact'] ."', '".$row['cphone']."', '".$row['twn']."', '".$row['postcode']."', '".$row['prefix']."', '".$row['twn']."',
            ".$roww['totalweightint'].", ".$roww['totalunits'].", '".$row['reqadd']."', '".$timin."', 0, 'Date Set', '".$result."', '".$row['ownerreq']."', '".$row['typ']."', '".$row['prev']."', '".$dataord['ord']."', '".$row['CollectionInstruction']."', '".$row['CollectionDate']."', '".$row['approved']."  ".$row['process']."', '".$row['approved']."', '".$row['process']."')
            
            set nocount on
            update Request_test2
            set confirmed = '0',
            laststatus = 'booked'
            where Request_ID ='".$_GET["rowid"]."'
            
            
            ";
            $fx = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/booknewrecsql.txt","a+");
            fwrite($fx,$bookrecord."\n");
            fclose($fx);


            
            $stmt = $conn->prepare($bookrecord);
            $stmt->execute();

            $fx = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/booknewrecsql.txt","a+");
            fwrite($fx,$bookrecord."\n");
            fclose($fx);


            echo "<meta http-equiv='refresh' content='0'>";

            }else{

              Echo "<p>in booked collections log</p>";
            }

            
          
          
            
            if ($stmtp === false) {
              // failed
            } else {
              $datechange = 1;
            }

            if($datechange===1){
              echo "
              <div id='myModal' class='modal fade' role='dialog'>
                <div class='modal-dialog'>
                  <div class='modal-content'>
                    <div class='modal-header'>
                      <button type='button' class='close' data-dismiss='modal'>&times;</button>
                      <h4 class='modal-title'>Modal Header</h4>
                    </div>
                    <div class='modal-body'>
                      <p>Changes Done Please Refresh.</p>
                    </div>
                    <div class='modal-footer'>
                      <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                    </div>
                  </div>
                </div>
              </div>
              ";
            }

            $fh = fopen($_SERVER["DOCUMENT_ROOT"]."/RS_Files/addingdatesql.txt","a+");
            fwrite($fh,$sqlUpdate."\n");
            fclose($fh);



       
          }
          ?>
          <br> 
          <br>

        </div>
    
    </div>
  </div>
</div>
</body>
</html>
